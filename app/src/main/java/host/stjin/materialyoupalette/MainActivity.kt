package host.stjin.materialyoupalette

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.view.HapticFeedbackConstants
import android.widget.GridLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children


class MainActivity : AppCompatActivity() {
    private var shouldTriggerRebirth = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val clipboard: ClipboardManager =
            getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val gridLayout = findViewById<GridLayout>(R.id.gridLayout)

        for (view in gridLayout.children) {
            val color = (view as TextView).background
            var colorInt = 0
            if (color is ColorDrawable)
                colorInt = color.color
            val text =
                Html.fromHtml(view.text.toString() + "<br><b>" + Integer.toHexString(colorInt) + "</b>")
            view.text = text

            view.setOnClickListener {
                val clip = ClipData.newPlainText(view.text.toString(), "#${Integer.toHexString(colorInt)}")
                clipboard.setPrimaryClip(clip)
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                Toast.makeText(this, "Hex copied to clipboard", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onPause() {
        super.onPause()
        shouldTriggerRebirth = true
    }

    override fun onResume() {
        super.onResume()
        if (shouldTriggerRebirth) {
            shouldTriggerRebirth = false
            triggerRebirth(this)
        }
    }

    private fun triggerRebirth(context: Context) {
        val packageManager: PackageManager = context.packageManager
        val intent = packageManager.getLaunchIntentForPackage(context.packageName)
        val componentName = intent!!.component
        val mainIntent = Intent.makeRestartActivityTask(componentName)
        context.startActivity(mainIntent)
        Runtime.getRuntime().exit(0)
    }
}